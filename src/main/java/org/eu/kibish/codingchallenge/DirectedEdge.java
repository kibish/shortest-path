package org.eu.kibish.codingchallenge;

class DirectedEdge {
    private final int sourceNodeId;
    private final int targetNodeId;
    private final double cost;

    public DirectedEdge(int sourceNodeId, int targetNodeId, double cost) {
        this.sourceNodeId = sourceNodeId;
        this.targetNodeId = targetNodeId;
        this.cost = cost;
    }

    public int getSourceNodeId() {
        return sourceNodeId;
    }

    public int getTargetNodeId() {
        return targetNodeId;
    }

    public double getCost() {
        return cost;
    }
}
