package org.eu.kibish.codingchallenge;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.PriorityQueue;
import java.util.Queue;

/**
 * original @author mephisto
 */
public class DijkstraShortestPath {

    class DistanceToEdge implements Comparable<DistanceToEdge> {
        
        private final int edge;
        private double distance;

        public DistanceToEdge(int edge, double distance) {
            this.edge = edge;
            this.distance = distance;
        }

        public double getDistance() {
            return distance;
        }

        public void setDistance(long distance) {
            this.distance = distance;
        }

        public int getEdge() {
            return edge;
        }

        @Override
        public int compareTo(DistanceToEdge param) {
            int cmp = new Double(distance).compareTo(param.getDistance());

            if (cmp == 0) {
                return new Integer(edge).compareTo(param.getEdge());
            }
            return 0;
        }
    }


    private DirectedEdge[] edgeTo;
    private Double[] distanceTo;
    private Queue<DistanceToEdge> priorityQueue;

    public DijkstraShortestPath(DirectedGraph graph, int source) {
        edgeTo = new DirectedEdge[graph.getNumberOfVertices()];
        distanceTo = new Double[graph.getNumberOfVertices()];
        priorityQueue = new PriorityQueue<>(graph.getNumberOfVertices());

        for (int v = 0; v < graph.getNumberOfVertices(); v++) {
            distanceTo[v] = Double.MAX_VALUE;
        }
        
        distanceTo[source] = 0.0;

        priorityQueue.offer(new DistanceToEdge(source, 0L));

        while (!priorityQueue.isEmpty()) {
            relax(graph, priorityQueue.poll().getEdge());
        }

    }

    private void relax(DirectedGraph graph, int v) {

        for (DirectedEdge edge : graph.getNeighborhoodList(v)) {
            int w = edge.getTargetNodeId();

            if (distanceTo[w] > distanceTo[v] + edge.getCost()) {
                distanceTo[w] = distanceTo[v] + edge.getCost();
                edgeTo[w] = edge;
                DistanceToEdge dte = new DistanceToEdge(w, distanceTo[w]);

                priorityQueue.remove(dte);
                priorityQueue.offer(dte);
            }
        }

    }

    public double getDistanceTo(int v) {
        return distanceTo[v];
    }

    public boolean hasPathTo(int v) {
        return distanceTo[v] < Long.MAX_VALUE;
    }

    public Iterable<DirectedEdge> getPathTo(int v) {
        Deque<DirectedEdge> path = new ArrayDeque<DirectedEdge>();
        
        if (!hasPathTo(v)) {
            return path;
        }
        
        for (DirectedEdge e = edgeTo[v]; e != null; e = edgeTo[e.getSourceNodeId()]) {
            path.push(e);
        }
        return path;
    }
}
