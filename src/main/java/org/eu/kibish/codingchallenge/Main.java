package org.eu.kibish.codingchallenge;

import java.net.URI;
import java.nio.file.Paths;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

public class Main {

    private static final String INPUT_FILE = "generatedGraph.json";
    private static final String NODE_DESTINATION = "b3-r7-r4nd7";
    private static final String NODE_START = "Erde";

    private static final String KEY_GRAPH_NODES = "nodes";
    private static final String KEY_GRAPH_EDGES = "edges";
    
    private static final String KEY_NODE_LABEL = "label";
    
    private static final String KEY_EDGE_SOURCE = "source";
    private static final String KEY_EDGE_TARGET = "target";
    private static final String KEY_EDGE_COST = "cost";
    
    public static void main(String[] args) {
        JSONArray nodesArray;
        JSONArray edgesArray;
        
        int destinationNodeId = -1;
        int startNodeId = -1;
        
        try {
            URI uri = URI.create("file://" + Paths.get(INPUT_FILE).toAbsolutePath());
            JSONTokener jsonTokener = new JSONTokener(uri.toURL().openStream());
            JSONObject jsonObject = new JSONObject(jsonTokener);

            nodesArray = jsonObject.getJSONArray(KEY_GRAPH_NODES);
            edgesArray = jsonObject.getJSONArray(KEY_GRAPH_EDGES);

            int id = 0;
            for (Object node : nodesArray) {
                if (node instanceof JSONObject) {
                    String nodeLabel = ((JSONObject) node).getString(KEY_NODE_LABEL);
                    
                    if (nodeLabel.equals(NODE_START)) {
                        startNodeId = id;
                    } else if (nodeLabel.equals(NODE_DESTINATION)) {
                        destinationNodeId = id;
                    }
                    
                    if (startNodeId > -1 && destinationNodeId > -1) {
                        break;
                    } else {
                        ++id;
                    }
                }
            } 

            if (startNodeId == -1) {
                throw new Exception("Can not find start node id.");
            }
            
            if (destinationNodeId == -1) {
                throw new Exception("Can not find destination node id.");
            }
            
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return;
        }


        DirectedGraph graph = new DirectedGraph(nodesArray.length());
        
        for (Object node : edgesArray) {
            if (node instanceof JSONObject) {
                int edgeSource = ((JSONObject) node).getInt(KEY_EDGE_SOURCE);
                int edgeTarget = ((JSONObject) node).getInt(KEY_EDGE_TARGET);
                double edgeCost = ((JSONObject) node).getDouble(KEY_EDGE_COST);
                
                graph.addEdge(new DirectedEdge(edgeSource, edgeTarget, edgeCost));
                graph.addEdge(new DirectedEdge(edgeTarget, edgeSource, edgeCost));
            }
        } 

        DijkstraShortestPath shortestPath = new DijkstraShortestPath(graph, startNodeId);

        JSONObject startNode = (JSONObject) nodesArray.get(startNodeId);
        JSONObject destNode = (JSONObject) nodesArray.get(destinationNodeId);

        if (shortestPath.hasPathTo(destinationNodeId)) {


            System.out.printf("%s to %s (%f)  ", 
                    startNode.getString(KEY_NODE_LABEL),
                    destNode.getString(KEY_NODE_LABEL),
                    shortestPath.getDistanceTo(destinationNodeId)
            );
            System.out.println();

            for (DirectedEdge edge : shortestPath.getPathTo(destinationNodeId)) {

                startNode = (JSONObject) nodesArray.get(edge.getSourceNodeId());
                destNode = (JSONObject) nodesArray.get(edge.getTargetNodeId());

                System.out.printf("%s->%s (%f)", 
                        startNode.getString(KEY_NODE_LABEL) ,
                        destNode.getString(KEY_NODE_LABEL),
                        edge.getCost());

                System.out.println();
            }

        } else {
            System.out.printf("%s to %s - no path found",
                    startNode.getString(KEY_NODE_LABEL),
                    destNode.getString(KEY_NODE_LABEL)
            );
        }
        
        System.out.println();
    }
}
