package org.eu.kibish.codingchallenge;

import java.util.ArrayList;
import java.util.List;

class DirectedGraph {
    private final int v;
    private int e;
    private final List<DirectedEdge>[] neighborhoodLists;

    public DirectedGraph(int v) {
        this.v = v;

        this.neighborhoodLists = (List<DirectedEdge>[]) new List[v];
        
        for (int i = 0; i < v; i++) {
            neighborhoodLists[i] = new ArrayList<>();
        }
        
        this.e = 0;
    }

    public int getNumberOfEdges() {
        return e;
    }

    public int getNumberOfVertices() {
        return v;
    }

    public void addEdge(DirectedEdge edge) {
        neighborhoodLists[edge.getSourceNodeId()].add(edge);
        e++;
    }

    public Iterable<DirectedEdge> getNeighborhoodList(int v) {
        return neighborhoodLists[v];
    }
}
